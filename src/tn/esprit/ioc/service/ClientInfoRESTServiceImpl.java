/*
 * Copyright 2017 by Walid YAICH <walid.yaich@esprit.tn>
 * This is an Open Source Software
 * License: http://www.gnu.org/licenses/gpl.html GPL version 3
 */

package tn.esprit.ioc.service;

public class ClientInfoRESTServiceImpl implements IClientInfoService{
	
	@Override
	public String getProjectTitleByClientId(int clientId){
		//Appel a un webservice REST
		return "Gestion des produits <From REST>" ;
	}
	
	@Override
	public String getFirstNameByClientId(int clientId){
		//Appel a un webservice REST
		return "Walid <From REST>";
	}
	
	@Override
	public String getLastNameByClientId(int clientId){
		//Appel a un webservice REST
		return "YAICH <From REST>";
	}

	@Override
	public String getProjectDetailsByClientId(int clientId) {
		//Appel a un webservice REST
		return "Les details du projet gestion de produits <From REST>";
	}
	
}
