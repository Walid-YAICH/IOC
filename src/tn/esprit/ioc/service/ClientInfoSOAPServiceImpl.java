/*
 * Copyright 2017 by Walid YAICH <walid.yaich@esprit.tn>
 * This is an Open Source Software
 * License: http://www.gnu.org/licenses/gpl.html GPL version 3
 */

package tn.esprit.ioc.service;

public class ClientInfoSOAPServiceImpl implements IClientInfoService{
	
	@Override
	public String getProjectTitleByClientId(int clientId){
		//Appel a un webservice SOAP
		return "Gestion des produits" ;
	}
	
	@Override
	public String getFirstNameByClientId(int clientId){
		//Appel a un webservice SOAP
		return "Walid";
	}
	
	@Override
	public String getLastNameByClientId(int clientId){
		//Appel a un webservice SOAP
		return "YAICH";
	}

	@Override
	public String getProjectDetailsByClientId(int clientId) {
		//Appel a un webservice SOAP
		return "Les details du projet gestion de produits";
	}
	
}
