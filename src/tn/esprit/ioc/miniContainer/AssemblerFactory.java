/*
 * Copyright 2017 by Walid YAICH <walid.yaich@esprit.tn>
 * This is an Open Source Software
 * License: http://www.gnu.org/licenses/gpl.html GPL version 3
 */

package tn.esprit.ioc.miniContainer;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Properties;

import tn.esprit.ioc.service.IClientInfoService;

public class AssemblerFactory {
	
	private static String configurationFilePath = "resources/config.properties";

	public static Object getObject(String pojoName) throws IOException, ClassNotFoundException, InstantiationException, IllegalAccessException, NoSuchMethodException, SecurityException, IllegalArgumentException, InvocationTargetException{
		//Charger le fichier properties
		Properties properties = new Properties();
		InputStream input = new FileInputStream(configurationFilePath);
		properties.load(input);
		//Récupérer les noms des classes et des méthodes d'un fichier properties
		String dependencyClassImplName = properties.getProperty("dependencyClassImplName") ;
		
		String classImplName = properties.getProperty("classImplName"); 
		if(classImplName.contains(pojoName)){ //Dans la vrai vie ce traitement est plus compliqué
		    String methodName = properties.getProperty("methodName") ;
		    
		    Class<?>[] methodParams = {IClientInfoService.class};
			
			//Préparer la dépendance, ceci est équivalent a :
			//IClientInfoService clientInfoSOAPService = new ClientInfoSOAPServiceImpl()
			Class<?> dependencyCls = Class.forName(dependencyClassImplName);
			Object dependencyObj = dependencyCls.newInstance();

			//Injecter la dépendance, ceci est équivalent a :
			//identityController.setiClientInfoService(clientInfoSOAPService)
			Class<?> cls = Class.forName(classImplName);
			Object obj = cls.newInstance();
			Method method = cls.getDeclaredMethod(methodName, methodParams);	
			method.invoke(obj, dependencyObj);
			return obj;
		}else{
			return null;
		}
	}
	
	private AssemblerFactory() {
	}
	
	/************** INJECTION DE DEPENDANCES PAR INSTANTIATION STATIQUE **************/
/*		//Préparer la dépendance
	IClientInfoService clientInfoSOAPService = new ClientInfoSOAPServiceImpl();
	//Injecter la dépendance dans IdentityController
	IidentityController identityController = new IdentityControllerImpl();
	identityController.setiClientInfoService(clientInfoSOAPService);
	//Injecter la dépendance dans ProjectController
	IprojectController projectController = new ProjectControllerImpl();
	projectController.setiClientInfoService(clientInfoSOAPService);
	
	//Maintenant que les objects sont assemblés, on peut exécuter les méthodes souhaitées 
	String clientFullName = identityController.getFullNameByClientId(clientId);
	String clientProject = projectController.getProjectByClientId(clientId);
	logger.info(clientFullName);
	logger.info(clientProject);*/

	/************** INJECTION DE DEPENDANCES PAR INSTANTIATION DYNAMIQUE **************/
//	String dependencyClassImplName = "tn.esprit.ioc.service.ClientInfoSOAPServiceImpl";
//    String classImplName = "tn.esprit.ioc.controller.IdentityControllerImpl";
//    String methodName = "setiClientInfoService" ;
	

}
