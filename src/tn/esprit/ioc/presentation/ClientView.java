/*
 * Copyright 2017 by Walid YAICH <walid.yaich@esprit.tn>
 * This is an Open Source Software
 * License: http://www.gnu.org/licenses/gpl.html GPL version 3
 */

package tn.esprit.ioc.presentation;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.logging.Logger;

import tn.esprit.ioc.controller.IidentityController;
import tn.esprit.ioc.miniContainer.AssemblerFactory;

public class ClientView {

    private static Logger logger = Logger.getGlobal();
	
	public static void main(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException, NoSuchMethodException, InvocationTargetException, IOException {
		int clientId = 7754654;
		
		//On demande a AssemblerFactory de nous fournir l'objet IdentityControllerImpl.
		//La classe AssemblerFactory va s'occuper de chercher les dépendances nécessaires, 
		//les instancier, les injecter, puis retourner l'objet demandé.
		IidentityController identityController = 
				(IidentityController) AssemblerFactory.getObject("IdentityControllerImpl");
		
		String clientFullName = identityController.getFullNameByClientId(clientId);
		logger.info(clientFullName); 
	}
}
