/*
 * Copyright 2017 by Walid YAICH <walid.yaich@esprit.tn>
 * This is an Open Source Software
 * License: http://www.gnu.org/licenses/gpl.html GPL version 3
 */

package tn.esprit.ioc.controller;

import tn.esprit.ioc.service.IClientInfoService;

public class ProjectControllerImpl implements IprojectController  {
	
	private IClientInfoService iClientInfoService;
	
	@Override
	public String getProjectByClientId(int clientId){
		return "Le projet de ce client est " + iClientInfoService.getProjectTitleByClientId(clientId) + 
				"\n" + iClientInfoService.getProjectDetailsByClientId(clientId); 
	}


	@Override
	public void setiClientInfoService(IClientInfoService iClientInfoService) {
		this.iClientInfoService = iClientInfoService;
	}

}
