/*
 * Copyright 2017 by Walid YAICH <walid.yaich@esprit.tn>
 * This is an Open Source Software
 * License: http://www.gnu.org/licenses/gpl.html GPL version 3
 */

package tn.esprit.ioc.controller;

import java.util.logging.Logger;

import tn.esprit.ioc.service.IClientInfoService;

public class IdentityControllerImpl implements IidentityController {
	
	private IClientInfoService iClientInfoService;
	
	private Logger logger = Logger.getGlobal();
	
	
	@Override
	public String getFullNameByClientId(int clientId){
		//Appeler le webservice SOAP
		return "Nom et prenom : " +  
				iClientInfoService.getFirstNameByClientId(clientId) + " " +
				iClientInfoService.getLastNameByClientId(clientId);
	}


	@Override
	public void setiClientInfoService(IClientInfoService iClientInfoService) {
		logger.info("setiClientInfoService run");
		this.iClientInfoService = iClientInfoService;
	}

}
