/*
 * Copyright 2017 by Walid YAICH <walid.yaich@esprit.tn>
 * This is an Open Source Software
 * License: http://www.gnu.org/licenses/gpl.html GPL version 3
 */

package tn.esprit.ioc.controller;

import tn.esprit.ioc.service.IClientInfoService;

public interface IprojectController {

	String getProjectByClientId(int clientId);
	void setiClientInfoService(IClientInfoService iClientInfoService);

}